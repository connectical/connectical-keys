#! /bin/bash
#
# update_root.bash
# Copyright (C) 2023 Óscar García Amor <ogarcia@connectical.com>
#
# Distributed under terms of the GNU GPLv3 license.
#

_sshd="/root/.ssh"
_ssha="${_sshd}/authorized_keys"
mkdir -p "${_sshd}"
rm -f ${_ssha}

for _dir in $(dirname ${0})/*; do
  [ -d "${_dir}" ] || continue
  cat "${_dir}"/*.pub >> "${_ssha}"
done

chmod 700 "${_sshd}"
chmod 600 "${_ssha}"
