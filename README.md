# Connectical Keys

Our public SSH Keys.

## Usage

Clone this repository on the machine where you want to deploy the keys. You
can run:

- `create_users.bash`: create the users, give them root permissions with
  `sudo` (`wheel` group) and install the SSH keys. If you need to change the
  `sudo` group use `SUDO_GROUP` variable. You can add users to additional
  groups with the variable `ADDITIONAL_GROUPS` (separated by commas). If the
  user exists, only his/her keys are updated.
  ```shell
  SUDO_GROUP=sudo ADDITIONAL_GROUPS=adm,disk,log ./create_users.bash
  ```
- `update_root.bash`: add the ssh keys to the `authorized_keys` of the root
  user.
