#! /bin/bash
#
# create_users.bash
# Copyright (C) 2023 Óscar García Amor <ogarcia@connectical.com>
#
# Distributed under terms of the GNU GPLv3 license.
#

SUDO_GROUP=${SUDO_GROUP:-wheel}

for _dir in $(dirname ${0})/*; do
  [ -d "${_dir}" ] || continue
  _name="${_dir##*/}"
  _home="/home/${_name}"
  _sshd="${_home}/.ssh"
  _ssha="${_sshd}/authorized_keys"
  id "${_name}" > /dev/null 2>&1 || {
    if [ -d "${_home}" ]; then
      useradd "${_name}"
    else
      useradd -m "${_name}"
    fi
    usermod -G "${SUDO_GROUP}" "${_name}"
    [ "${ADDITIONAL_GROUPS}" != "" ] && \
      usermod -a -G "${ADDITIONAL_GROUPS}" "${_name}"
  }
  mkdir -p "${_sshd}"
  cat "${_dir}"/*.pub > "${_ssha}"
  chown -R "${_name}":"${_name}" "${_home}"
  chmod 700 "${_sshd}"
  chmod 600 "${_ssha}"
done
